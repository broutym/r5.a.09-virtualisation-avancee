#!/bin/bash

# Vérifie si la variable est définie et non vide
if [ -z "${NOM+x}" ]; then
  echo "La variable NOM n'est pas définie."
  exit 2
fi
$@
