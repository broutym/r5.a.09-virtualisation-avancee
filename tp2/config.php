<?php

try
{
    $db="${database}";
    $dbhost="${host}";
    $dbport="${port}";
    $dbuser="${user}";
    $dbpasswd="${password}";
     
    $pdo = new PDO('mysql:host='.$dbhost.';port='.$dbport.';dbname='.$db.'', $dbuser, $dbpasswd);
    
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
?>
